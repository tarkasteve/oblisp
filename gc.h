
#ifndef _GC_H
#define _GC_H

#include "lisp.h"

#define DEFAULT_HEAPSIZE (1024 * 1024 * 128)
#define DEFAULT_STACKSIZE (1024 * 1024 * 8)

typedef struct _space {
    lispobj_t *start;
    lispobj_t *end;
    size_t size;
} space_t;

typedef struct _lispmem {
    space_t hfrom;
    space_t hto;
    space_t stack;
} lispmem_t;

/* typedef struct _mem_obj { */
/*     enum { MOBJ_FORWARD, MOBJ_OTHER } type; */
/*     size_t size; */
/* } mem_obj_t; */


/* ********************************************************************** */

extern lispmem_t gc_init(size_t heap_size, size_t stack_size);
extern void gc_shutdown(lispmem_t mem);

extern void* heap_alloc_lisp(lispmem_t *mem);
extern lispobj_t* stack_alloc_lisp(lispmem_t *mem);
extern int stack_push_lisp(lispmem_t *mem, lispobj_t obj);
extern int stack_pop_lisp(lispmem_t *mem, lispobj_t *obj);

extern void full_gc(lispmem_t *mem);

#endif
