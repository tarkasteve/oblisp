
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "parser.h"

inline void init_input_str(input_t *in, char *str)
{
    in->type = IN_STRING;
    in->data.str_data.str = str;
    in->data.str_data.pos = 0;
}

char str_readchar(input_t *in)
{
    char c = in->data.str_data.str[in->data.str_data.pos];
    if (c != 0) {
        in->data.str_data.pos++;
    }
    return c;
}

void str_back(input_t *in)
{
    if (in->data.str_data.pos > 0)
        in->data.str_data.pos--;
}


char str_peek(input_t *in)
{
    return in->data.str_data.str[in->data.str_data.pos];
}


char readchar(input_t *in)
{
    switch (in->type) {
      case IN_STRING:
        return str_readchar(in);

      default:
        printf("UNKNOWN TYPE\n");
        exit(-1);
    }
}

void back(input_t *in)
{
    switch (in->type) {
      case IN_STRING:
        str_back(in);
        break;

      default:
        printf("UNKNOWN TYPE\n");
        exit(-1);
    }
}


char peek(input_t *in)
{
    switch (in->type) {
      case IN_STRING:
        return str_peek(in);

      default:
        printf("UNKNOWN TYPE\n");
        exit(-1);
    }
}



lispobj_t* nextexpr(input_t *in)
{
    return 0;
}

lispobj_t* parsestr(char *str)
{
    input_t in;
    init_input_str(&in, str);

    return nextexpr(&in);
}
