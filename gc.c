
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "gc.h"

#define GC_EXTRA_CHECKS 1

#define CHECKMALLOC(r) if (r == 0) { printf("malloc failed\n"); exit(-1); }


int stack_push_lisp(lispmem_t *mem, lispobj_t obj)
{
    if (mem->stack.end + sizeof(lispobj_t) > mem->stack.end + mem->stack.size) {
        return -1;
    }
    *mem->stack.end = obj;
    mem->stack.end++;
    return 0;
}

lispobj_t* stack_alloc_lisp(lispmem_t *mem)
{
    // FIXME
   if (mem->stack.end + sizeof(lispobj_t) > mem->stack.end + mem->stack.size) {
       return (lispobj_t*)-1;
   }
    return mem->stack.end++;
}


int stack_pop_lisp(lispmem_t *mem, lispobj_t *obj)
{

    if (mem->stack.end - 1 < mem->stack.start) {
        printf("POP failed: %p - %zx < %p\n",
               mem->stack.end, sizeof(lispobj_t), mem->stack.start);
       return -1;
   }
    mem->stack.end -= sizeof(lispobj_t);
    if (obj != 0)
        *obj = *mem->stack.end;
    return 0;
}

void* heap_alloc_lisp(lispmem_t *mem)
{
    // FIXME: Better size tracking
    if ((mem->hfrom.end + 1) > (mem->hfrom.start + (mem->hfrom.size/sizeof(lispobj_t)))) {
        return (void*)-1;  // OOM
    }

    // Alloc
    lispobj_t *new = mem->hfrom.end++;

    return new;
}

lispobj_t* copy_or_forward(lispmem_t *mem, lispobj_t *obj) {
    if (obj == 0)
        return obj;

    if (obj->type == OBJ_FORWARD) {
        return obj->data.forward;
    }

    // Copy
    lispobj_t *new = mem->hto.end++;
    *new = *obj;

    // Replace with forward
    obj->type = OBJ_FORWARD;
    obj->data.forward = new;

    return new;
}

void gc_space(lispmem_t *mem, space_t *space)
{
//    printf("GC SPACE\n");
    for (lispobj_t *obj = space->start; obj < space->end; obj++) {
//        printf("    CHECK\n");
        if (obj->type == OBJ_CONS) {
            obj->data.cons.car = copy_or_forward(mem, obj->data.cons.car);
            obj->data.cons.cdr = copy_or_forward(mem, obj->data.cons.cdr);
        }
    }
}

void full_gc(lispmem_t *mem)
{
    // Reset To space
    mem->hto.end = mem->hto.start;

    // Traverse stack
    gc_space(mem, &(mem->stack));

    // Traverse To space
    gc_space(mem, &(mem->hto));

    // Swap spaces
    space_t tmp = mem->hfrom;
    mem->hfrom = mem->hto;
    mem->hto = tmp;
}


lispmem_t gc_init(size_t heap_size, size_t stack_size)
{
    lispmem_t mem;

    mem.hfrom.start = malloc(heap_size);
    CHECKMALLOC(mem.hfrom.start);
    mem.hfrom.end = mem.hfrom.start;
    mem.hfrom.size = heap_size;

    mem.hto.start = malloc(heap_size);
    CHECKMALLOC(mem.hto.start);
    mem.hto.end = mem.hto.start;
    mem.hto.size = heap_size;

    mem.stack.start = malloc(stack_size);
    CHECKMALLOC(mem.stack.start);
    mem.stack.end = mem.stack.start;
    mem.stack.size = stack_size;

    return mem;
}

void gc_shutdown(lispmem_t mem)
{
    free(mem.hfrom.start);
    free(mem.hto.start);
    free(mem.stack.start);
}
