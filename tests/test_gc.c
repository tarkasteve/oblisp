
#include <stdlib.h>
#include "CuTest.h"
#include "../gc.h"
#include "../lisp.h"

void test_gc_overflow(CuTest *tc)
{
    lispmem_t mem = gc_init(DEFAULT_HEAPSIZE, DEFAULT_STACKSIZE);

    size_t num = DEFAULT_HEAPSIZE / sizeof(lispobj_t);
    for (int i = 0; i < num; i++) {
        void *ret = heap_alloc_lisp(&mem);
        CuAssertTrue(tc, 0 < ret);
    }
    CuAssertTrue(tc, (lispobj_t*)-1 == heap_alloc_lisp(&mem));

    gc_shutdown(mem);
}


void test_gc_reclaim_one(CuTest *tc)
{
    lispmem_t mem = gc_init(DEFAULT_HEAPSIZE, DEFAULT_STACKSIZE);

    lispobj_t *obj = heap_alloc_lisp(&mem);
    CuAssertTrue(tc, obj != 0);
    CuAssertTrue(tc, ((void*)mem.hfrom.end - ((void*)mem.hfrom.start) == sizeof(lispobj_t)));

    full_gc(&mem);

    CuAssertTrue(tc, mem.hfrom.end == mem.hfrom.start);

    gc_shutdown(mem);
}

void test_gc_one_on_stack(CuTest *tc)
{
    lispmem_t mem = gc_init(DEFAULT_HEAPSIZE, DEFAULT_STACKSIZE);

    lispobj_t *obj = heap_alloc_lisp(&mem);
    CuAssertTrue(tc, obj != 0);

    lispobj_t* sobj = stack_alloc_lisp(&mem);
    sobj->type = OBJ_CONS;
    sobj->data.cons.car = obj;

    full_gc(&mem);

    // Check one in heap
    CuAssertTrue(tc, ((void*)mem.hfrom.end - ((void*)mem.hfrom.start) == sizeof(lispobj_t)));

    gc_shutdown(mem);
}

void test_gc_one_off_stack(CuTest *tc)
{
    lispmem_t mem = gc_init(DEFAULT_HEAPSIZE, DEFAULT_STACKSIZE);

    lispobj_t *obj = heap_alloc_lisp(&mem);
    CuAssertTrue(tc, obj != 0);

    lispobj_t* sobj = stack_alloc_lisp(&mem);
    cons_init(sobj, obj, 0);

    stack_pop_lisp(&mem, 0);

    full_gc(&mem);

    // Check one in heap
    CuAssertTrue(tc, mem.hfrom.end == mem.hfrom.start);

    gc_shutdown(mem);
}


void test_gc_full_to_empty(CuTest *tc)
{
    lispmem_t mem = gc_init(DEFAULT_HEAPSIZE, DEFAULT_STACKSIZE);

    size_t num = DEFAULT_HEAPSIZE / sizeof(lispobj_t);
    for (int i = 0; i < num; i++) {
        heap_alloc_lisp(&mem);
    }

    full_gc(&mem);

    CuAssertTrue(tc, mem.hfrom.end == mem.hfrom.start);

    gc_shutdown(mem);
}


void test_gc_list(CuTest *tc)
{
    lispmem_t mem = gc_init(DEFAULT_HEAPSIZE, DEFAULT_STACKSIZE);

    lispobj_t* sobj = stack_alloc_lisp(&mem);

    lispobj_t* prev = heap_alloc_lisp(&mem);
    cons_init(sobj, 0, prev);

    for (int i = 0; i < 3; i++) {
        lispobj_t *obj = heap_alloc_lisp(&mem);
        cons_init(prev, 0, obj);
        prev = obj;
    }
    cons_init(prev, 0, 0);

    full_gc(&mem);

    // Check all in heap
    CuAssertIntEquals(tc, 4, mem.hfrom.end - mem.hfrom.start);

    gc_shutdown(mem);
}


void test_gc_reverse_list(CuTest *tc)
{
    lispmem_t mem = gc_init(DEFAULT_HEAPSIZE, DEFAULT_STACKSIZE);

    lispobj_t* prev = heap_alloc_lisp(&mem);
    cons_init(prev, 0, 0);
    for (int i = 0; i < 3; i++) {
        lispobj_t *obj = heap_alloc_lisp(&mem);
        cons_init(obj, 0, prev);
        prev = obj;
    }

    lispobj_t* sobj = stack_alloc_lisp(&mem);
    cons_init(sobj, prev, 0);

    full_gc(&mem);

    // Check one in heap
    CuAssertIntEquals(tc, 4, mem.hfrom.end - mem.hfrom.start);

    gc_shutdown(mem);
}



// ********************************************************************** //

CuSuite* gc_suite (void)
{
    CuSuite* suite = CuSuiteNew();
    SUITE_ADD_TEST(suite, test_gc_overflow);
    SUITE_ADD_TEST(suite, test_gc_reclaim_one);
    SUITE_ADD_TEST(suite, test_gc_one_on_stack);
    SUITE_ADD_TEST(suite, test_gc_full_to_empty);
    SUITE_ADD_TEST(suite, test_gc_list);
    SUITE_ADD_TEST(suite, test_gc_reverse_list);
    SUITE_ADD_TEST(suite, test_gc_one_off_stack);
//    SUITE_ADD_TEST(suite, test_gc_);

    return suite;
}
