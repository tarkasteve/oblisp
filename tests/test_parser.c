
#include <stdlib.h>
#include "CuTest.h"
#include "../parser.h"

void test_parse_int(CuTest *tc)
{
    lispobj_t *obj = parsestr("1");
    CuAssertPtrNotNull(tc, obj);
}


void test_parse_string(CuTest *tc)
{
    lispobj_t *obj = parsestr("\"1\"");
    CuAssertPtrNotNull(tc, obj);
}


void test_str_input(CuTest *tc)
{
    char *str = "abcdef";
    input_t in;
    init_input_str(&in, str);

    CuAssertTrue(tc, readchar(&in) == 'a');

    CuAssertTrue(tc, peek(&in) == 'b');
    CuAssertTrue(tc, readchar(&in) == 'b');

    CuAssertTrue(tc, readchar(&in) == 'c');
    CuAssertTrue(tc, readchar(&in) == 'd');
    CuAssertTrue(tc, readchar(&in) == 'e');
    CuAssertTrue(tc, readchar(&in) == 'f');
    CuAssertTrue(tc, readchar(&in) == 0);
    CuAssertTrue(tc, readchar(&in) == 0);

    back(&in);
    CuAssertTrue(tc, peek(&in) == 'f');
    back(&in);
    CuAssertTrue(tc, peek(&in) == 'e');

}



// ********************************************************************** //

CuSuite* parser_suite (void)
{
    CuSuite* suite = CuSuiteNew();
//    SUITE_ADD_TEST(suite, test_parse_int);
//    SUITE_ADD_TEST(suite, test_parse_string);
    SUITE_ADD_TEST(suite, test_str_input);
    return suite;
}
