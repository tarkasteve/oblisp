
#include <stdio.h>
#include "CuTest.h"

CuSuite* parser_suite();
CuSuite* gc_suite();

void run_all(void) {
    CuString *output = CuStringNew();
    CuSuite* suite = CuSuiteNew();

    CuSuiteAddSuite(suite, parser_suite());
    CuSuiteAddSuite(suite, gc_suite());

    CuSuiteRun(suite);
    CuSuiteSummary(suite, output);
    CuSuiteDetails(suite, output);
    printf("%s\n", output->buffer);
}

int main(void) {
    run_all();
    return 0;
}
