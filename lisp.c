
#include "lisp.h"

void cons_init(lispobj_t *o, lispobj_t *car, lispobj_t *cdr)
{
    o->type = OBJ_CONS;
    o->data.cons.car = car;
    o->data.cons.cdr = cdr;
}
