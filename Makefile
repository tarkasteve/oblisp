TARGET = oblisp
SOURCES = $(wildcard *.c)
OBJS = $(SOURCES:.c=.o)

TEST_TARGET = tests/test
TEST_SOURCES = $(wildcard tests/*.c)
TEST_OBJS = $(TEST_SOURCES:.c=.o) $(filter-out main.o, $(OBJS))
TESTS = $(TEST_SOURCES:.c=)
TEST_CFLAGS = 
TEST_LDFLAGS = 

CC = gcc
CFLAGS = -g -Wall --std=gnu99
LDFLAGS =  

all: deps.mk $(TARGET)

clean:
	rm -f $(OBJS) $(TEST_OBJS) $(TARGET) deps.mk *~ */*~ $(TEST_TARGET)

$(TEST_TARGET): $(TEST_OBJS)
	$(CC) $(TEST_OBJS) $(LDFLAGS) -o $@

.PHONY: test
test: deps.mk $(TEST_TARGET)
	./$(TEST_TARGET)


deps.mk:
	$(CC) -MM $(SOURCES) $(TEST_SOURCES) > deps.mk

$(TARGET): $(OBJS)
	$(CC) $(OBJS) $(CFLAGS) $(LDFLAGS) -o $@

-include deps.mk
