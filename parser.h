
#ifndef _PARSER_H
#define _PARSER_H

//enum The
typedef struct _lispobj {
    void *nothing;
} lispobj_t;


typedef struct _input {
    enum input_type { IN_STRING, IN_FD } type;
    union {
        struct _str_data {
            char *str;
            size_t pos;
        } str_data;
        struct _int_data {
            int fd;
        } int_data;
    } data;
} input_t;


extern void init_input_str(input_t *in, char *str);
extern lispobj_t* parsestr(char *str);

/* ********************************************************************** */

// For testing
extern char readchar(input_t *in);
extern char peek(input_t *in);
extern void back(input_t *in);


#endif
