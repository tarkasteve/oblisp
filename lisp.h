
#ifndef _LISP_H
#define _LISP_H

#include <unistd.h>

struct _lispobj {
    size_t size;

    enum _type {
        OBJ_CONS,
        OBJ_INT,
        OBJ_FORWARD
    } type;

    union {
        struct {
            struct _lispobj *car;
            struct _lispobj *cdr;
        } cons;

        int integer;

        struct _lispobj *forward;

    } data;
};

typedef struct _lispobj lispobj_t;

/* ********************************************************************** */

extern void cons_init(lispobj_t *o, lispobj_t *car, lispobj_t *cdr);

#endif
